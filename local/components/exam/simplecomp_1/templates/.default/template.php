<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<ul>
    <? foreach ($arResult['NEWS_ELEMENTS'] as $news): ?>
        <li>
            <b><?= $news['NAME'] ?></b> - <?= $news['ACTIVE_FROM'] ?>
            <? $sectionNames = "" ?>
            <? foreach ($news['CATALOG_SECTIONS_IDS'] as $sectionId): ?>
                <? $sectionNames .= "{$arResult['CATALOG_SECTIONS'][$sectionId]['NAME']}, " ?>
            <? endforeach; ?>
            (<?= substr($sectionNames, 0, -2) ?>)
            <ul>
                <? foreach ($news['CATALOG_SECTIONS_IDS'] as $sectionId): ?>
                    <? foreach ($arResult['CATALOG_SECTIONS'][$sectionId]['ELEMENTS'] as $element): ?>
                        <li>
                            <?= $element['NAME'] ?> -
                            <?= $element['PROPERTY_PRICE_VALUE'] ?> -
                            <?= $element['PROPERTY_MATERIAL_VALUE'] ?> -
                            <?= $element['PROPERTY_ARTNUMBER_VALUE'] ?>
                        </li>
                    <? endforeach; ?>
                <? endforeach; ?>
            </ul>
        </li>
    <? endforeach; ?>
</ul>