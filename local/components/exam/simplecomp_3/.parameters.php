<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$arComponentParameters = array(
    "PARAMETERS" => array(
        "IBLOCK_NEWS_ID" => array(
            "NAME" => GetMessage("SIMPLECOMP_3:PARAMS:NEWS"),
            "TYPE" => "STRING",
            "DEFAULT" => "1"
        ),
        "NEWS_AUTHOR_CODE" => array(
            "NAME" => GetMessage("SIMPLECOMP_3:PARAMS:NEWS_AUTHOR_CODE"),
            "TYPE" => "STRING",
            "DEFAULT" => "AUTHOR"
        ),
        "USER_AUTHOR_CODE" => array(
            "NAME" => GetMessage("SIMPLECOMP_3:PARAMS:USER_AUTHOR_CODE"),
            "TYPE" => "STRING",
            "DEFAULT" => "UF_AUTHOR_TYPE"
        ),
        "CACHE_TIME" => Array(
            "DEFAULT" => 36000000
        )
    )
);