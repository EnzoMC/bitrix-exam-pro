<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

if ($arParams['IBLOCK_CANONICAL_ID']) {
    $elements = CIBlockElement::GetList(
        [],
        [
            'IBLOCK_ID' => $arParams['IBLOCK_CANONICAL_ID'],
            '=PROPERTY_NEWS' => $arResult['ID']
        ],
        false,
        false,
        ['ID', 'IBLOCK_ID', 'NAME']
    );
    if ($element = $elements->GetNext()) {
        $comp = $this->__component;
        if (is_object($comp)) {
            $comp->arResult['CANONICAL'] = $element['NAME'];
            $comp->SetResultCacheKeys(['CANONICAL']);
        }
    }
}