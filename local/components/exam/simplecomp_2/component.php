<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$arParams["CACHE_TIME"] = $_GET['F'] ? 0 : $arParams["CACHE_TIME"];
$arParams['CACHE_TYPE'] = 'A';
if ($arParams["VENDOR_COUNT"]) {
    $arNavParams = ["nPageSize" => $arParams["VENDOR_COUNT"]];
    $arNavigation = CDBResult::GetNavParams($arNavParams);
} else {
    $arNavParams = false;
    $arNavigation = false;
}

if ($this->startResultCache(false, [$USER->GetGroups(), $arNavigation])) {
    if (!CModule::includeModule("iblock")) {
        $this->abortResultCache();
        return;
    }
    $vendorOrder = [];
    $vendorFilter = [
        'IBLOCK_ID' => $arParams['IBLOCK_VENDOR_ID'],
        'ACTIVE' => 'Y',
        'CHECK_PERMISSIONS' => 'Y'
    ];
    $vendorNav = $arNavParams;
    $vendorSelect = ['ID', 'NAME', 'IBLOCK_ID'];
    $obVendorElements = CIBlockElement::GetList(
        $vendorOrder,
        $vendorFilter,
        false,
        $vendorNav,
        $vendorSelect
    );
    $arResult["NAV_STRING"] = $obVendorElements->GetPageNavStringEx($navComponentObject, 'Разделы');
    $arVendors = [];
    while ($vendor = $obVendorElements->GetNext()) {
        $arVendors[$vendor['ID']] = $vendor['ID'];
        $arResult['VENDOR_ELEMENTS'][] = $vendor;
    }
    $catalogOrder = ['name' => 'asc', 'sort' => 'asc'];
    $catalogFilter = [
        'IBLOCK_ID' => $arParams['IBLOCK_CATALOG_ID'],
        'ACTIVE' => 'Y',
        'CHECK_PERMISSIONS' => 'Y',
        "PROPERTY_{$arParams['VENDOR_CODE']}" => $arVendors
    ];
    if ($_GET['F']) {
        $catalogFilter[] = [
            "LOGIC" => "OR",
            ["<=PROPERTY_PRICE" => '1700', "=PROPERTY_MATERIAL" => "Дерево, ткань"],
            ["<PROPERTY_PRICE" => '1500', "=PROPERTY_MATERIAL" => "Металл, пластик"],
        ];
    }
    $catalogSelect = [
        'ID',
        'NAME',
        'IBLOCK_ID',
        'IBLOCK_SECTION_ID'
    ];
    $obCatalogElements = CIBlockElement::GetList(
        $catalogOrder,
        $catalogFilter,
        false,
        false,
        $catalogSelect
    );
    $arResult['PRICE'] = [
        'MIN' => 10000000,
        'MAX' => 0
    ];
    $obCatalogElements->SetUrlTemplates($arParams["DETAIL_URL"]);
    while ($obElement = $obCatalogElements->GetNextElement()) {
        $element = $obElement->GetFields();
        $element['PROPERTIES'] = [
            'PRICE' => $obElement->GetProperty('PRICE')['VALUE'],
            'MATERIAL' => $obElement->GetProperty('MATERIAL')['VALUE'],
            'ARTNUMBER' => $obElement->GetProperty('ARTNUMBER')['VALUE'],
            $arParams['VENDOR_CODE'] => $obElement->GetProperty($arParams['VENDOR_CODE'])['VALUE'],
        ];
        if ($element['PROPERTIES'][$arParams['VENDOR_CODE']]) {
            $tmp = [];
            foreach ($element['PROPERTIES'][$arParams['VENDOR_CODE']] as $item) {
                $tmp[$item] = $item;
            }
            $element['PROPERTIES'][$arParams['VENDOR_CODE']] = $tmp;
        }
        if ($element['PROPERTIES']['PRICE'] > $arResult['PRICE']['MAX']) {
            $arResult['PRICE']['MAX'] = $element['PROPERTIES']['PRICE'];
        }
        if ($element['PROPERTIES']['PRICE'] < $arResult['PRICE']['MIN']) {
            $arResult['PRICE']['MIN'] = $element['PROPERTIES']['PRICE'];
        }
        $arButtons = CIBlock::GetPanelButtons($element["IBLOCK_ID"], $element["ID"], 0, []);
        $element['EDIT_LINK'] = $arButtons["edit"]["edit_element"]["ACTION_URL"];
        $element['DELETE_LINK'] = $arButtons["edit"]["delete_element"]["ACTION_URL"];
        $arResult['CATALOG_ELEMENTS'][] = $element;
    }
    $arResult['VENDOR_ELEMENTS_COUNT'] = count($arResult['VENDOR_ELEMENTS']);
    $this->SetResultCacheKeys(['VENDOR_ELEMENTS_COUNT']);
    $this->includeComponentTemplate();
}
$APPLICATION->SetTitle(GetMessage('SIMPLECOMP_2:COMPONENT:TITLE') . ': ' . $arResult['VENDOR_ELEMENTS_COUNT']);
if ($APPLICATION->GetShowIncludeAreas()) {
    $arButtons = CIBlock::GetPanelButtons($arParams['IBLOCK_CATALOG_ID'], 0, 0, []);
    $addElementButton = $arButtons[$APPLICATION->GetPublicShowMode()]['add_element'];
    $getElementListButton = array_merge(
        $arButtons['submenu']['element_list'],
        [
            'ACTION' => $arButtons['submenu']['element_list']['ACTION_URL'],
            'TITLE' => "ИБ в админке",
            'IN_PARAMS_MENU' => true,
            'IN_MENU' => false
        ]
    );
    $this->addIncludeAreaIcons(CIBlock::GetComponentMenu('', ['' => [$getElementListButton, $addElementButton]]));
}