<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<b><?= GetMessage('SIMPLECOMP_2:PARAMS:TEMPLATE:FILTER') ?>:</b><br/>
<a href="<?= $APPLICATION->GetCurPage() ?>?F=1">С фильтром</a><br>
<a href="<?= $APPLICATION->GetCurPage() ?>">Без фильтра</a><br>
<hr/>
<?= GetMessage("SIMPLECOMP_2:PARAMS:TEMPLATE:TIMESTAMP") ?>: <?= time(); ?>
<hr/>
<b><?= GetMessage('SIMPLECOMP_2:PARAMS:TEMPLATE:TITLE') ?>:</b>
<ul>
    <? foreach ($arResult['VENDOR_ELEMENTS'] as $vendor): ?>
        <li>
            <b><?= $vendor['NAME'] ?></b>
            <ul>
                <? foreach ($arResult['CATALOG_ELEMENTS'] as $element): ?>
                    <? if ($element['PROPERTIES'][$arParams['VENDOR_CODE']][$vendor['ID']]): ?>
                        <?
                        $this->AddEditAction($element['ID'], $element['EDIT_LINK'], CIBlock::GetArrayByID($element["IBLOCK_ID"], "ELEMENT_EDIT"));
                        $this->AddDeleteAction($element['ID'], $element['DELETE_LINK'], CIBlock::GetArrayByID($element["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => 'Удалить товар?'));
                        ?>
                        <li id="<?= $this->GetEditAreaId($element['ID']); ?>">
                            <?= $element['NAME'] ?> - <?= $element['PROPERTIES']['PRICE'] ?> -
                            <?= $element['PROPERTIES']['MATERIAL'] ?> -
                            <?= $element['PROPERTIES']['ARTNUMBER'] ?><br/>
                            <?= $element['DETAIL_PAGE_URL'] ?>
                        </li>
                    <? endif; ?>
                <? endforeach; ?>
            </ul>
        </li>
    <? endforeach; ?>
</ul>
<? $this->SetViewTarget('catalog_prices') ?>
    <div style="color:red; margin: 34px 15px 35px 15px">
        <?= GetMessage('SIMPLECOMP_2:PARAMS:TEMPLATE:MAX_PRICE') ?>: <?= $arResult['PRICE']['MAX'] ?><br><?= GetMessage('SIMPLECOMP_2:PARAMS:TEMPLATE:MIN_PRICE') ?>: <?= $arResult['PRICE']['MIN'] ?>
    </div>
<? $this->EndViewTarget() ?>
<?= $arResult["NAV_STRING"] ?>