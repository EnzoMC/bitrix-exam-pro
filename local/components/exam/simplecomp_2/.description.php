<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$arComponentDescription = array(
    "NAME" => GetMessage("SIMPLECOMP_2:DESC:NAME"),
    "DESCRIPTION" => GetMessage("SIMPLECOMP_2:DESC:NAME:DESCRIPTION"),
    'PATH' => [
        'ID' => 'content',
    ],
);

?>