<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
if ($arParams['META_SPECIALDATE'] == 'Y') {
    $comp = $this->__component;
    if (is_object($comp)) {
        $comp->arResult['META_SPECIALDATE_VALUE'] = $arResult['ITEMS'][0]['DISPLAY_ACTIVE_FROM'];
        $comp->SetResultCacheKeys(['META_SPECIALDATE_VALUE']);
    }
}
