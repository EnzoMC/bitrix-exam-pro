<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
if ($arParams['META_SPECIALDATE'] == 'Y' && isset($arResult['META_SPECIALDATE_VALUE'])) {
    $APPLICATION->SetPageProperty('specialdate', $arResult['META_SPECIALDATE_VALUE']);
}
