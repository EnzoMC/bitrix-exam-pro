$(document).on('click', '#report_link', function () {
    var $form = $('form#report_news');
    var $data = {};
    $data['report_ajax'] = $form.find('input[name="report_ajax"]').val();
    if ($data['report_ajax']) {
        $.get(
            $form.attr('action'),
            $data,
            function (data) {
                setAddText(data);
            }
        );
    } else {
        $form.submit();
    }
});

function setAddText(data) {
    $('#report_text').html(data);
}