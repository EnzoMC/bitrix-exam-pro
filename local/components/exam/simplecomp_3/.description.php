<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$arComponentDescription = array(
    "NAME" => GetMessage('SIMPLECOMP_3:DESC:NAME'),
    "DESCRIPTION" => GetMessage('SIMPLECOMP_3:DESC:DESC'),
    'PATH' => [
        'ID' => 'content',
    ],
);
