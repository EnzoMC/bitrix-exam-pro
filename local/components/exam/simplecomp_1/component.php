<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

if ($this->startResultCache()) {
    if (!CModule::IncludeModule("iblock")) {
        $this->abortResultCache();
        return;
    }
    $arSections = [];
    $arNews = [];
    $obCatalogSections = CIBlockSection::GetList(
        [],
        ['IBLOCK_ID' => $arParams['IBLOCK_CATALOG_ID'], 'ACTIVE' => 'Y', "!{$arParams['NEWS_CODE']}" => false],
        false,
        ['ID', 'IBLOCK_ID', 'IBLOCK_SECTION_ID', 'NAME', $arParams['NEWS_CODE']],
        false
    );
    while ($section = $obCatalogSections->GetNext()) {
        $tmp = [];
        foreach ($section[$arParams['NEWS_CODE']] as $item) {
            $arNews[$item] = $item;
            $tmp[$item] = $item;
        }
        $section[$arParams['NEWS_CODE']] = $tmp;
        $arSections[$section['ID']] = $section['ID'];
        $arResult['CATALOG_SECTIONS'][$section['ID']] = $section;
    }
    $obCatalogElements = CIBlockElement::GetList(
        [],
        ['IBLOCK_ID' => $arParams['IBLOCK_CATALOG_ID'], 'SECTION_ID' => $arSections, 'ACTIVE' => 'Y'],
        false,
        false,
        ['ID', 'IBLOCK_ID', 'IBLOCK_SECTION_ID', 'NAME', 'PROPERTY_PRICE', 'PROPERTY_MATERIAL', 'PROPERTY_ARTNUMBER']
    );
    while ($element = $obCatalogElements->GetNext()) {
        $arResult['CATALOG_SECTIONS'][$element['IBLOCK_SECTION_ID']]['ELEMENTS'][$element['ID']] = $element;
        $arResult['CATALOG_ELEMENTS_COUNT']++;
    }
    $obNewsElements = CIBlockElement::GetList(
        [],
        ['IBLOCK_ID' => $arParams['IBLOCK_NEWS_ID'], 'ACTIVE' => 'Y', 'ID' => $arNews],
        false,
        false,
        ['ID', 'IBLOCK_ID', 'NAME', 'ACTIVE_FROM']
    );
    while ($news = $obNewsElements->GetNext()) {
        $arResult['NEWS_ELEMENTS'][$news['ID']] = $news;
    }
    foreach ($arResult['NEWS_ELEMENTS'] as $key => $newsElement) {
        foreach ($arResult['CATALOG_SECTIONS'] as $catalogSection) {
            if ($catalogSection[$arParams['NEWS_CODE']][$newsElement['ID']]) {
                $arResult['NEWS_ELEMENTS'][$key]['CATALOG_SECTIONS_IDS'][] = $catalogSection['ID'];
            }
        }
    }
    $this->setResultCacheKeys(['CATALOG_ELEMENTS_COUNT']);
    $this->includeComponentTemplate();
}
$APPLICATION->SetTitle(GetMessage('SIMPLECOMP_1:ELEMENTS_COUNT') . ' - ' . $arResult['CATALOG_ELEMENTS_COUNT']);
