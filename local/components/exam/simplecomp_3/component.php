<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

foreach ($arParams as $key => $param) {
    $arParams[$key] = trim($param);
}

if ($USER->IsAuthorized()) {
    $curUser = CUser::GetByID($USER->GetID())->Fetch();
    if ($this->startResultCache(false, [$curUser[$arParams['USER_AUTHOR_CODE']]])) {
        if (!CModule::includeModule("iblock")) {
            $this->abortResultCache();
            return;
        }
        $obUsers = CUser::GetList(
            $by = "id",
            $order = "asc",
            [$arParams['USER_AUTHOR_CODE'] => $curUser[$arParams['USER_AUTHOR_CODE']]],
            ['SELECT' => [$arParams['USER_AUTHOR_CODE']], 'FIELDS' => ['ID', 'LOGIN']]
        );
        $arUsers = [];
        while ($user = $obUsers->Fetch()) {
            if ($user['ID'] !== $curUser['ID']) {
                $arResult['USERS_LIST'][] = $user;
                $arUsers[$user['ID']] = $user['ID'];
            }
        }
        $obNewsElements = CIBlockElement::GetList(
            [],
            [
                'IBLOCK_ID' => $arParams['IBLOCK_NEWS_ID'],
                'ACTIVE' => 'Y',
                "PROPERTY_{$arParams['NEWS_AUTHOR_CODE']}" => $arUsers,
            ],
            false,
            false,
            ['ID', 'IBLOCK_ID', 'NAME', 'ACTIVE_FROM']
        );
        while ($obNews = $obNewsElements->GetNextElement()) {
            $news = $obNews->GetFields();
            $news[$arParams['NEWS_AUTHOR_CODE']] = $obNews->GetProperty($arParams['NEWS_AUTHOR_CODE'])['VALUE'];
            if ($news[$arParams['NEWS_AUTHOR_CODE']]) {
                $tmp = [];
                foreach ($news[$arParams['NEWS_AUTHOR_CODE']] as $item) {
                    $tmp[$item] = $item;
                }
                $news[$arParams['NEWS_AUTHOR_CODE']] = $tmp;
            }
            if ($news[$arParams['NEWS_AUTHOR_CODE']][$curUser['ID']]) {
                continue;
            }
            $arResult['NEWS_ELEMENTS'][] = $news;
        }
        $arResult['NEWS_ELEMENTS_COUNT'] = count($arResult['NEWS_ELEMENTS']);
        $this->SetResultCacheKeys(['NEWS_ELEMENTS_COUNT']);
        $this->includeComponentTemplate();
    }
    $APPLICATION->SetTitle(GetMessage('SIMPLECOMP_3:COMPONENT:TITLE') . ' - ' . $arResult['NEWS_ELEMENTS_COUNT']);
}
