<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

if ($arParams['IBLOCK_CANONICAL_ID'] && isset($arResult['CANONICAL'])) {
    $APPLICATION->SetPageProperty('canonical', $arResult['CANONICAL']);
}
if (isset($_GET['report_ajax'])) {
    if($_GET['report_ajax']){
        $APPLICATION->RestartBuffer();
    }
    $obElement = new CIBlockElement();
    $elementId = $obElement->Add([
        'ACTIVE' => 'Y',
        'IBLOCK_ID' => IBLOCK_REPORTS_ID,
        'NAME' => time(),
        'DATE_ACTIVE_FROM' => date('d.m.y H:i:s',time()),
        'PROPERTY_VALUES' => [
            'USER' => $USER->IsAuthorized() ? "{$USER->GetID()} - {$USER->GetLogin()} - {$USER->GetFullName()}" : "Не авторизован",
            'NEWS' => $arResult['ID']
        ]
    ]);
    $report = $elementId ? "Ваше мнение учтено, №$elementId" : "Ошибка!";
    if($_GET['report_ajax']){
        echo $report;
        die();
    }
    else{
        echo "<script type='text/javascript'>setAddText('$report');</script>";
    }
}
CJSCore::Init(['jquery']);