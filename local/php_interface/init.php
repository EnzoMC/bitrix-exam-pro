<?php
define('IBLOCK_PRODUCTS_ID', 2);
define('IBLOCK_METATAGS_ID', 6);
define('IBLOCK_REPORTS_ID', 7);
define('USER_CONTENT_MANAGER_GROUP_ID', 5);
define('USER_ADMINISTRATOR_GROUP_ID', 1);

AddEventHandler('iblock', 'OnBeforeIBlockElementUpdate', 'StopDeactivation');
AddEventHandler('main', 'OnBeforeEventAdd', 'ChangeFeedbackAuthor');
AddEventHandler('main', 'OnPageStart', 'SetSeoTags');
AddEventHandler('main', 'OnBuildGlobalMenu', 'SetContentManagerAdminMenu');

function StopDeactivation(&$arFields)
{
    if (!CModule::IncludeModule('iblock')) {
        ShowError('Отсутствует модуль инфоблока');
        return false;
    }
    if ($arFields['IBLOCK_ID'] == IBLOCK_PRODUCTS_ID && $arFields['ACTIVE'] == 'N') {
        $element = CIBlockElement::GetList(
            [],
            ['ID' => $arFields['ID'], 'IBLOCK_ID' => IBLOCK_PRODUCTS_ID, '>SHOW_COUNTER' => '2', 'ACTIVE' => 'Y'],
            false,
            false,
            ['ID', 'IBLOCK_ID', 'ACTIVE', 'SHOW_COUNTER']
        )->Fetch();
        if ($element) {
            global $APPLICATION;
            $APPLICATION->throwException("Товар невозможно деактивировать, у него {$element['SHOW_COUNTER']} просмотров ");
            return false;
        }
    }
    return true;
}

function ChangeFeedbackAuthor(&$event, &$lid, &$arFields)
{
    if ($event == 'FEEDBACK_FORM') {
        global $USER;
        if ($USER->IsAuthorized()) {
            $arFields['AUTHOR'] = "Пользователь авторизован: {$USER->GetID()} ({$USER->GetLogin()}) {$USER->GetFullName()}, данные из формы: {$arFields['AUTHOR']}";
        } else {
            $arFields['AUTHOR'] = "Пользователь не авторизован, данные из формы: {$arFields['AUTHOR']}";
        }
        CEventLog::Add([
            'AUDIT_TYPE_ID' => 'CHANGE_AUTHOR',
            'DESCRIPTION' => "Замена данных в отсылаемом письме -{$arFields['AUTHOR']}"
        ]);
    }
}


function SetSeoTags()
{
    global $APPLICATION;
    $curPage = $APPLICATION->GetCurPage();
    $title = '';
    $desc = '';
    $obCache = new \CPHPCache();
    if ($obCache->InitCache(3600, $curPage, '/seo/')) {
        $vars = $obCache->GetVars();
        $title = $vars['title'];
        $desc = $vars['description'];
    } else {
        if (CModule::IncludeModule('iblock')) {
            $element = CIBlockElement::GetList(
                [],
                ['NAME' => $curPage, 'IBLOCK_ID' => IBLOCK_METATAGS_ID],
                false,
                false,
                ['ID', 'PROPERTY_TITLE', 'PROPERTY_DESCRIPTION']
            )->Fetch();
            $title = $element['PROPERTY_TITLE_VALUE'];
            $desc = $element['PROPERTY_DESCRIPTION_VALUE'];
            $obCache->StartDataCache(3600, $curPage, '/seo/');
            $obCache->EndDataCache(['title' => $title, 'description' => $desc]);
        }
    }
    if ($title) {
        $APPLICATION->SetPageProperty('title', $title);
    }
    if ($desc) {
        $APPLICATION->SetPageProperty('description', $desc);
    }
}

function SetContentManagerAdminMenu(&$aGlobalMenu, &$aModuleMenu)
{
    global $USER;
    if (in_array(USER_CONTENT_MANAGER_GROUP_ID, CUser::GetUserGroup($USER->GetID()))) {
        foreach ($aModuleMenu as $item) {
            if ($item['items_id'] == 'menu_iblock_/news') {
                $aModuleMenu = [$item];
                break;
            }
        }
        $aGlobalMenu = ['global_menu_content' => $aGlobalMenu['global_menu_content']];
    }
}

function CheckUserCount()
{
    $lastDate = COption::GetOptionString('main', 'check_user_count_day');
    $currentDate = date('d.m.Y H:i:s', time());
    $empty = '';
    $orderUsers = ['id' => 'asc'];
    $count = CUser::GetList($orderUsers, $empty, ['DATE_REGISTER_1' => $lastDate, 'DATE_REGISTER_2' => $currentDate], ['FIELDS' => ['ID', 'DATE_REGISTER']])->SelectedRowsCount();
    $days = $lastDate ? floor((strtotime($currentDate) - strtotime($lastDate)) / (60 * 60 * 24)) : '-';
    $obUser = CUser::GetList($orderUsers, $empty, ['GROUPS_ID' => [USER_ADMINISTRATOR_GROUP_ID]], ['FIELDS' => ['ID', 'EMAIL']]);
    while ($user = $obUser->Fetch()) {
        CEvent::Send(
            'CHECK_USERS_COUNT',
            SITE_ID,
            [
                'COUNT' => $count,
                'DAYS' => $days,
                'EMAIL_TO' => $user['EMAIL']
            ]
        );
    }
    COption::SetOptionString('main', 'check_user_count_day', $currentDate);
    return 'CheckUserCount();';
}