<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<h2><?= GetMessage('SIMPLECOMP_3:TEMPLATE:TITLE')?></h2>
<ul>
    <? foreach ($arResult['USERS_LIST'] as $user): ?>
        <li>
            [<?= $user['ID'] ?>] - <?= $user['LOGIN'] ?>
            <ul>
                <? foreach ($arResult['NEWS_ELEMENTS'] as $news): ?>
                    <? if ($news[$arParams['NEWS_AUTHOR_CODE']][$user['ID']]): ?>
                        <li>
                            <?= $news['ACTIVE_FROM'] ?> - <?= $news['NAME'] ?>
                        </li>
                    <? endif; ?>
                <? endforeach; ?>
            </ul>
        </li>
    <? endforeach; ?>
</ul>
