<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$arComponentDescription = array(
    "NAME" => GetMessage("SIMPLECOMP_1:DESC:NAME"),
    "DESCRIPTION" => GetMessage("SIMPLECOMP_1:DESC:DESCRIPTION"),
    'PATH' => array(
        'ID' => 'content',
    ),
);