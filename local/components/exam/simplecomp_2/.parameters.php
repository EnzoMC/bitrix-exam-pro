<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$arComponentParameters = array(
    "PARAMETERS" => array(
        "IBLOCK_CATALOG_ID" => array(
            "NAME" => GetMessage("SIMPLECOMP_2:PARAMS:CATALOG"),
            "TYPE" => "STRING",
            "DEFAULT" => "2"
        ),
        "IBLOCK_VENDOR_ID" => array(
            "NAME" => GetMessage("SIMPLECOMP_2:PARAMS:VENDOR"),
            "TYPE" => "STRING",
            "DEFAULT" => "8"
        ),
        "VENDOR_CODE" => array(
            "NAME" => GetMessage("SIMPLECOMP_2:PARAMS:VENDOR_CODE"),
            "TYPE" => "STRING",
            "DEFAULT" => "VENDOR"
        ),
        "CACHE_TIME" => Array(
            "DEFAULT" => 36000000
        ),
        "DETAIL_URL" => array(
            "NAME" => GetMessage("SIMPLECOMP_2:PARAMS:DETAIL_URL"),
            "TYPE" => "STRING",
            "DEFAULT" => "/ex2/simplecomp_2/#SECTION_ID#/#ELEMENT_ID#/"
        ),
        "VENDOR_COUNT" => array(
            "NAME" => GetMessage("SIMPLECOMP_2:PARAMS:COUNT"),
            "TYPE" => "STRING",
            "DEFAULT" => "2",
        ),
    ),
);
?>