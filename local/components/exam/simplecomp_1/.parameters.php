<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$arComponentParameters = array(
    "PARAMETERS" => array(
        "IBLOCK_CATALOG_ID" => array(
            "NAME" => GetMessage('SIMPLECOMP_1:PARAMS:CATALOG'),
            "TYPE" => "STRING",
            "DEFAULT" => "2"
        ),
        "IBLOCK_NEWS_ID" => array(
            "NAME" => GetMessage('SIMPLECOMP_1:PARAMS:NEWS'),
            "TYPE" => "STRING",
            "DEFAULT" => "1"
        ),
        "NEWS_CODE" => array(
            "NAME" => GetMessage('SIMPLECOMP_1:PARAMS:CODE'),
            "TYPE" => "STRING",
            "DEFAULT" => "UF_NEWS_LINK"
        ),
        "CACHE_TIME" => Array(
            "DEFAULT" => 36000000
        ),
    ),
);