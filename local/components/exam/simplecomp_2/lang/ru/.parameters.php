<?php
$MESS["SIMPLECOMP_2:PARAMS:CATALOG"] = "ID инфоблока с каталогом товаров";
$MESS["SIMPLECOMP_2:PARAMS:VENDOR"] = "ID инфоблока с классификатором";
$MESS["SIMPLECOMP_2:PARAMS:VENDOR_CODE"] = "Код свойства товара, в котором хранится привязка товара к классификатору";
$MESS["SIMPLECOMP_2:PARAMS:DETAIL_URL"] = "Шаблон ссылки на детальный просмотр товара";
$MESS["SIMPLECOMP_2:PARAMS:COUNT"] = "Количество элементов классификатора на странице";