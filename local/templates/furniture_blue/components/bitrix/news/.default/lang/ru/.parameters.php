<?php
$MESS ['T_IBLOCK_DESC_NEWS_DATE'] = "Выводить дату элемента";
$MESS ['T_IBLOCK_DESC_NEWS_NAME'] = "Выводить название элемента";
$MESS ['T_IBLOCK_DESC_NEWS_PICTURE'] = "Выводить изображение для анонса";
$MESS ['T_IBLOCK_DESC_NEWS_TEXT'] = "Выводить текст анонса";
$MESS ['META_SPECIALDATE_NAME'] = "Установить значение тега specialdate";
$MESS ['IBLOCK_CANONICAL_ID_NAME'] = "ID информационного блока для rel=canonical";
$MESS ['REPORT_AJAX:NAME'] = "Собирать жалобы в режиме AJAX";